import { Routes, RouterModule } from '@angular/router';
import{ArchivosComponent} from '../app/pages/archivos/archivos.component'



const appRoutes: Routes = [
    {
        path:'nueva',
        loadChildren: './pages/comunicaciones/comunicacion/comunicacion.module#ComunicacionModule'
    },
    {
        path: '**',
        redirectTo: '/nueva',
    }
];


export const routing = RouterModule.forRoot(appRoutes);