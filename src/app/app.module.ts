import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { NgxEditorModule } from 'ngx-editor';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { TooltipModule } from 'ngx-bootstrap/tooltip';

import {routing} from './app.routing';
import {AuthGuard} from './guard/auth.guard';

import { BsDropdownModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import {LoginComponent} from './component/login/login.component';
import { AlertComponent } from './directive/alert/alert.component';
import { SidebarComponent } from './component/sidebar/sidebar.component';

import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { NavbarComponent } from './component/navbar/navbar.component';
import {PagesModule} from '../app/pages/pages.module';

const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true
};


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AlertComponent,
    SidebarComponent,
    NavbarComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    BsDropdownModule.forRoot(),
    routing,
    PerfectScrollbarModule,
    NgxEditorModule,
    AngularFontAwesomeModule,
    TooltipModule,
    HttpClientModule,
    PagesModule
  ],
  providers: [AuthGuard,{
    provide: PERFECT_SCROLLBAR_CONFIG,
    useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
