import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  toggled = false;
  _hasBackgroundImage = true;
  menus = [
    {
      title: 'general',
      type: 'header'
    },
    {
      title: 'Comunicaciones',
      icon: 'fa fa-tachometer-alt',
      active: false,
      type: 'dropdown',
      badge: {
        text: 'New ',
        class: 'badge-warning'
      },
      submenus: [
        {
          title: 'nueva',
          path:'/archivos',
          badge: {
            text: 'Pro ',
            class: 'badge-success'
          }
        },
        {
          title: 'plantilla',
          path:'/buscar',
          badge: {
            text: 'Pro ',
            class: 'badge-success'
          }
        },
        {
          title: 'buscar',
          badge: {
            text: 'Pro ',
            class: 'badge-success'
          }
        }
      ]
    },
    {
      title: 'Salidas',
      icon: 'fa fa-shopping-cart',
      active: false,
      type: 'dropdown',
      badge: {
        text: '3',
        class: 'badge-danger'
      },
      submenus: [
        {
          title: 'nueva',
        },
        {
          title: 'plantilla'
        }
      ]
    },
    {
      title: 'Clientes',
      icon: 'far fa-gem',
      active: false,
      type: 'dropdown',
      submenus: [
        {
          title: 'Agregar',
        },
        {
          title: 'Mostrar'
        },
        {
          title: 'Actualizar'
        }
      ]
    },
    {
      title: 'Departamentos',
      icon: 'fa fa-chart-line',
      active: false,
      type: 'dropdown',
      submenus: [
        {
          title: 'Agregar',
        },
        {
          title: 'Actualizar'
        },
        {
          title: 'Detalle'
        }
      ]
    },
    {
      title: 'Concentrador',
      icon: 'fa fa-globe',
      active: false,
      type: 'dropdown',
      submenus: [
        {
          title: 'Detalle',
        }
      ]
    },
    {
      title: 'KPI',
      type: 'header'
    },
    {
      title: 'Concentrador',
      icon: 'fa fa-book',
      active: false,
      type: 'simple',
      badge: {
        text: 'Beta',
        class: 'badge-primary'
      },
    },
    {
      title: 'Test7',
      icon: 'fa fa-calendar',
      active: false,
      type: 'simple'
    },
    {
      title: 'Test8',
      icon: 'fa fa-folder',
      active: false,
      type: 'simple'
    }
  ];
  constructor() { }

  toggle() {
    this.toggled = ! this.toggled;
  }

  getSidebarState() {
    return this.toggled;
  }

  setSidebarState(state: boolean) {
    this.toggled = state;
  }

  getMenuList() {
    return this.menus;
  }

  get hasBackgroundImage() {
    return this._hasBackgroundImage;
  }

  set hasBackgroundImage(hasBackgroundImage) {
    this._hasBackgroundImage = hasBackgroundImage;
  }
}