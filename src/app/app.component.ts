import { Component } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  location: Location;
  title = 'rpa';

  constructor(location: Location){
    this.location = location;
  }

//   getTitle() {
//     let titlee: any = this.location.prepareExternalUrl(this.location.path());
//     for (let i = 0; i < this.listTitles.length; i++) {
//         if (this.listTitles[i].type === "link" && this.listTitles[i].path === titlee) {
//             return this.listTitles[i].title;
//         } else if (this.listTitles[i].type === "sub") {
//             for (let j = 0; j < this.listTitles[i].children.length; j++) {
//                 let subtitle = this.listTitles[i].path + '/' + this.listTitles[i].children[j].path;
//                 if (subtitle === titlee) {
//                     return this.listTitles[i].children[j].title;
//                 }
//             }
//         }
//     }
//     return 'Dashboard';
// }

}
