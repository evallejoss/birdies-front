import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComunicacionRoutingModule } from './comunicacion-routing.module';
import { ComunicacionComponent } from './comunicacion.component';

@NgModule({
  declarations: [ComunicacionComponent],
  imports: [
    CommonModule,
    ComunicacionRoutingModule
  ]
})
export class ComunicacionModule { }
