import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import{ArchivosComponent} from '../pages/archivos/archivos.component'

@NgModule({
  declarations: [
    ArchivosComponent,
  ],
  exports:[
    ArchivosComponent,
  ],
  imports: [
    CommonModule
  ]
})
export class PagesModule { }
