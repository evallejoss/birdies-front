import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BuscarRoutingModule } from './buscar-routing.module';
import { BuscarComponent } from './buscar.component';
import { PostComponent } from './post/post.component';

@NgModule({
  declarations: [BuscarComponent, PostComponent],
  imports: [
    CommonModule,
    BuscarRoutingModule
  ]
})
export class BuscarModule { }
