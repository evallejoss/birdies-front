import { Component, OnInit } from '@angular/core';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-buscar',
  templateUrl: './buscar.component.html',
  styleUrls: ['./buscar.component.css']
})
export class BuscarComponent implements OnInit {

  mensajes : any;

  constructor( private dataService : DataService) { }

  ngOnInit() {

    this.mensajes = this.dataService.getPost();
    // .subscribe( (post : any []) => {
    //   console.log(post)
    //   this.mensajes = post;
    // })

  }

  escuchaEvento(id : number){
    console.log('mostrar id', id)
  }

}
