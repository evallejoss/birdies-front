import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';


@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private router: Router) {
    
   }
   

  canActivate(
    route: ActivatedRouteSnapshot,state: RouterStateSnapshot) {
      console.log('validacion')
      if (localStorage.getItem('currentUser')) {
        // logged in so return true
        console.log('true')
        return true;
      }else{
        // not logged in so redirect to login page with the return url
        console.log('false')
      this.router.navigate(['/login']);
      return false;
      }
    
  }
}
